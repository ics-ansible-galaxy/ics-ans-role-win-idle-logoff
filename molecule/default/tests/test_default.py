import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_logoff(host):
    host.ansible("raw", "Get-WmiObject Win32_Product -ComputerName localhost | select Name,Version | select-string 'Lithnet'", check=False)["stdout_lines"][0]
#    assert x == "@{Name=Lithnet Idle Logoff Utility; Version=1.2.8134.23029}"
