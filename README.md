# ics-ans-role-win-idle-logoff

Ansible role to install Lithnet Idle Logoff.
Github - https://github.com/lithnet/idle-logoff

The app signs out a logged in Windows user after a period of user inactivity.
This is to ensure systems are available to users with full system resources.

## Role Variables

```yaml
...
win_idlelogoff_timer: 30
win_idlelogoff_toggle: Enabled
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-win-idle-logoff
```

## License

BSD 2-clause
